const faker = require('faker')

describe('Test suite', function () {
    it('Retrieving all clients', function () {
        cy.request('http://rbt-course:8080/hotel-rest/webresources/client/')
    })

    it('Retrieving all clients and checking the status code', function () {
        cy.request('http://rbt-course:8080/hotel-rest/webresources/client/').its('status').should('eq', 200)
    })

    it('Retrieving a specific client and checking status code', function () {
        cy.request('http://rbt-course:8080/hotel-rest/webresources/client/1').its('status').should('eq', 200)
    })


    it('Retrieving a specific client and printing the status code', function () {
        cy.request('http://rbt-course:8080/hotel-rest/webresources/client/1').then((response => {
            cy.log(response.status)
        }))
    })

    it('Retrieving a specific client and asserting that the response object contains certain properties', function () {
        cy.request('http://rbt-course:8080/hotel-rest/webresources/client/1').then((response => {
            expect(response).to.have.property('status')
            expect(response).to.have.property('body')
            expect(response).to.have.property('headers')
            expect(response).to.have.property('body')
        }))
    })

    it('Retrieving a specific client, converting the response object into a string and printing its value and datatype', function () {
        cy.request('http://rbt-course:8080/hotel-rest/webresources/client/1').then((response => {
            const responseAsString = JSON.stringify(response)
            cy.log(responseAsString)
            cy.log(typeof responseAsString)
        }))

    })

    // it('Creating a new client with a static JSON object', function () {
    //     cy.request({
    //         method: 'POST',
    //         url: 'http://rbt-course:8080/hotel-rest/webresources/client/',
    //         headers: {
    //             'Accept-Encoding': 'gzip, deflate, br',
    //             'Content-Type': 'application/json',
    //             'Accept': 'application/json'
    //         },
    //         body: {                 
    //             "name": "rafael silva",
    //             "createDate": 1451617200000,
    //             "email": "rafael.silvaa@emaill.com",
    //             "gender": "M",
    //             "socialSecurityNumber": "14190844"                
    //         }
    //     }).then((response => {
    //         expect(response.status).to.eq(204)
    //     }))
    // })


    // npm install faker -> To install the required library
    // const faker = require('faker') -> to make the library available from this file.
    it('Creating a new client using faker library', function () {
        const fakeName = faker.name.firstName()
        const fakeEmailAddress = faker.internet.email()
        const fakeSocialSecurityNumber = faker.random.number(1000000)

        //Creating a client with the first request
        cy.request({
            method: 'POST',
            url: 'http://rbt-course:8080/hotel-rest/webresources/client/',
            headers: {
                'Accept-Encoding': 'gzip, deflate, br',
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            },
            body: {
                "name": fakeName,
                "createDate": 1451617200000,
                "email": fakeEmailAddress,
                "gender": "M",
                "socialSecurityNumber": fakeSocialSecurityNumber
            }
        }).then((response => {
            expect(response.status).to.eq(204)
        }))

        // Making a second request to assert that the name is registered in the database
        cy.request('http://rbt-course:8080/hotel-rest/webresources/client/').then((response => {
            const responseAsJson = JSON.stringify(response)
            expect(responseAsJson).to.have.string(fakeName)
            expect(responseAsJson).to.have.string(fakeEmailAddress)
            expect(responseAsJson).to.have.string(fakeSocialSecurityNumber)
        }))
    })

    it('Retrieving all the clients and printing the information of the last client added into the database', function () {
        cy.request('http://rbt-course:8080/hotel-rest/webresources/client/').then((response => {
            cy.log(response.body[response.body.length - 1].id)
            cy.log(response.body[response.body.length - 1].createDate)
            cy.log(response.body[response.body.length - 1].email)
            cy.log(response.body[response.body.length - 1].gender)
            cy.log(response.body[response.body.length - 1].socialSecurityNumber)
        }))
    })

    it('Creating a new client with random data and deleting it', function () {
        const fakeName = faker.name.firstName()
        const fakeEmailAddress = faker.internet.email()
        const fakeSocialSecurityNumber = faker.random.number(1000000)

        //Creating a client with the first request
        cy.request({
            method: 'POST',
            url: 'http://rbt-course:8080/hotel-rest/webresources/client/',
            headers: {
                'Accept-Encoding': 'gzip, deflate, br',
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            },
            body: {
                "name": fakeName,
                "createDate": 1451617200000,
                "email": fakeEmailAddress,
                "gender": "M",
                "socialSecurityNumber": fakeSocialSecurityNumber
            }
        }).then((response => {
            expect(response.status).to.eq(204)
        }))

        // Making a second request to assert that the name is registered in the database
        cy.request('http://rbt-course:8080/hotel-rest/webresources/client/').then((response => {
            const responseAsJson = JSON.stringify(response)
            expect(responseAsJson).to.have.string(fakeName)
            expect(responseAsJson).to.have.string(fakeEmailAddress)
            expect(responseAsJson).to.have.string(fakeSocialSecurityNumber)

            const idOfLastUser = response.body[response.body.length - 1].id

            // Making a third request for deleting the client
            cy.request({
                method: 'DELETE',
                url: `http://rbt-course:8080/hotel-rest/webresources/client/${idOfLastUser}`,
                headers: {
                    'Accept-Encoding': 'gzip, deflate, br',
                    'Content-Type': 'application/json',
                    'Accept': 'application/json'
                }
            }).then((response => {
                expect(response.status).to.eq(204)
                const responseAsJson = JSON.stringify(response)
                expect(responseAsJson).to.not.have.string(fakeName)
                expect(responseAsJson).to.not.have.string(fakeEmailAddress)
                expect(responseAsJson).to.not.have.string(fakeSocialSecurityNumber)
            }))
        }))
    })
})
